package com.li.cyy.demo.bean;

import org.springframework.context.annotation.Bean;

import javax.persistence.Entity;

public class T_notice {
    private int id;
    private String text_content;
    private String article_url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText_content() {
        return text_content;
    }

    public void setText_content(String text_content) {
        this.text_content = text_content;
    }

    public String getArticle_url() {
        return article_url;
    }

    public void setArticle_url(String article_url) {
        this.article_url = article_url;
    }
}
