package com.li.cyy.demo.bean;

import java.util.Date;

public class T_team_recruitment {
    private int id;
    private int team_id;
    private Date release_time;
    private int publisher_id;
    private String title;
    private String text_content;
    private int image_set_id;
    private String demand;
    private String statu;
    private int get_number;
    private int need_number;
    private int approver_id;
    private int views;
    private int collection_volume;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public Date getRelease_time() {
        return release_time;
    }

    public void setRelease_time(Date release_time) {
        this.release_time = release_time;
    }

    public int getPublisher_id() {
        return publisher_id;
    }

    public void setPublisher_id(int publisher_id) {
        this.publisher_id = publisher_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText_content() {
        return text_content;
    }

    public void setText_content(String text_content) {
        this.text_content = text_content;
    }

    public int getImage_set_id() {
        return image_set_id;
    }

    public void setImage_set_id(int image_set_id) {
        this.image_set_id = image_set_id;
    }

    public String getDemand() {
        return demand;
    }

    public void setDemand(String demand) {
        this.demand = demand;
    }

    public String getStatu() {
        return statu;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public int getGet_number() {
        return get_number;
    }

    public void setGet_number(int get_number) {
        this.get_number = get_number;
    }

    public int getNeed_number() {
        return need_number;
    }

    public void setNeed_number(int need_number) {
        this.need_number = need_number;
    }

    public int getApprover_id() {
        return approver_id;
    }

    public void setApprover_id(int approver_id) {
        this.approver_id = approver_id;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getCollection_volume() {
        return collection_volume;
    }

    public void setCollection_volume(int collection_volume) {
        this.collection_volume = collection_volume;
    }
}
