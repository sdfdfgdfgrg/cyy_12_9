package com.li.cyy.demo.bean;

import java.util.Date;

public class T_news {
    private int id;
    private String table_name;
    private int tuple_id;
    private Date release_time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public int getTuple_id() {
        return tuple_id;
    }

    public void setTuple_id(int tuple_id) {
        this.tuple_id = tuple_id;
    }

    public Date getRelease_time() {
        return release_time;
    }

    public void setRelease_time(Date release_time) {
        this.release_time = release_time;
    }
}
