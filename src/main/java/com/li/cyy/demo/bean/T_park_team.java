package com.li.cyy.demo.bean;

public class T_park_team {
    private int id;
    private String name;
    private String type;
    private String logo_url;
    private String room_url;
    private String main_direction;
    private String team_profile;
    private String team_orverview;
    private int views;
    private int collection_volume;

    public String getTeam_profile() {
        return team_profile;
    }

    public void setTeam_profile(String team_profile) {
        this.team_profile = team_profile;
    }

    public String getTeam_orverview() {
        return team_orverview;
    }

    public void setTeam_orverview(String team_orverview) {
        this.team_orverview = team_orverview;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getCollection_volume() {
        return collection_volume;
    }

    public void setCollection_volume(int collection_volume) {
        this.collection_volume = collection_volume;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public String getRoom_url() {
        return room_url;
    }

    public void setRoom_url(String room_url) {
        this.room_url = room_url;
    }

    public String getMain_direction() {
        return main_direction;
    }

    public void setMain_direction(String main_direction) {
        this.main_direction = main_direction;
    }
}
