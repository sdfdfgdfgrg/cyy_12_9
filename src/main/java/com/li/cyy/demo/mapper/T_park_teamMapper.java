package com.li.cyy.demo.mapper;

import com.li.cyy.demo.bean.T_notice;
import com.li.cyy.demo.bean.T_park_team;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface T_park_teamMapper {
    @Select("select * from t_park_team")
    public List<T_park_team> findAll();
    //按团队类查询多个
    @Select("select * from t_park_team where type = #{type}")
    public List<T_park_team> findByType(String type);
    //获取数据总数
    @Select("select count(*) from t_park_team")
    public int getCount();
    //分页查询
    @Select("select * from t_park_team limit #{pageNo},#{pageSize}")
    public List<T_park_team> findPage(int pageNo,int pageSize);

    //查询单个
    @Select("select * from t_park_team where id=#{id}")
    T_park_team findById(int id);

    @Delete("delete from t_park_team where id=#{id}")
    public int delTeam(int id);

    @Update("update t_park_team set name=#{name},type=#{type},logo_url=#{logo_url},main_direction=#{main_direction} where id=#{id}")
    int update1(int id,String name,String type,String logo_url,String main_direction);

    @Update("update t_park_team set name=#{name},type=#{type},room_url=#{room_url},main_direction=#{main_direction} where id=#{id}")
    int update2(int id,String name,String type,String room_url,String main_direction);

    @Insert("insert into t_park_team(name,type,logo_url,room_url,main_direction) values (#{name},#{type},#{logo_url},#{room_url},#{main_direction})")
    int add(String name,String type,String logo_url,String room_url,String main_direction);
}
