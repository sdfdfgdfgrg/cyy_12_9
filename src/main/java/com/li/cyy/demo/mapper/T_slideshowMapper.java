package com.li.cyy.demo.mapper;

import com.li.cyy.demo.bean.T_notice;
import com.li.cyy.demo.bean.T_slideshow;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface T_slideshowMapper {
    @Select("select * from t_slideshow")
    public List<T_slideshow> findAll();
}
