package com.li.cyy.demo.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface ImageMapper {

    @Insert("insert into image(name,url) values (#{name},#{url})")
    public  int addImage(String name, String url);
}
