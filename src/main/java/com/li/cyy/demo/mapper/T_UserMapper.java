package com.li.cyy.demo.mapper;

import com.li.cyy.demo.bean.T_user;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
@Mapper
public interface T_UserMapper {
    @Select("select * from t_user where username = #{username} and password = #{password}")
    public  T_user  qUserByUserPWD(T_user t_user);
}
