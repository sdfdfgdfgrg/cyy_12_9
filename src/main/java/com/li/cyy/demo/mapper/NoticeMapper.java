package com.li.cyy.demo.mapper;

import com.li.cyy.demo.bean.T_notice;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface NoticeMapper {
//    查询所有t_notice
    @Select("select * from t_notice")
    public List<T_notice> findAll();
    //查询单个
    @Select("select * from t_notice where id=#{id}")
    T_notice findById(int id);

    @Delete("delete from t_notice where id=#{id}")
    public int delNotice(int id);

    @Update("update t_notice set text_content=#{text_content},article_url=#{article_url} where id=#{id}")
    int update(int id,String text_content,String article_url);

    @Update("update t_notice set text_content=#{text_content},article_url=#{article_url} where id=#{id}")
    int update2(T_notice t_notice);

    @Insert("insert into t_notice(text_content,article_url) values (#{text_content},#{article_url})")
    int add(T_notice t_notice);
}
