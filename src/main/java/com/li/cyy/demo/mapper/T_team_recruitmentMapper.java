package com.li.cyy.demo.mapper;

import com.li.cyy.demo.bean.T_team_recruitment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface T_team_recruitmentMapper {
    @Select("select * from t_team_recruitment")
    List<T_team_recruitment> findAll();
    //指定编号的元组数据
    @Select("select * from t_team_recruitment where id = #{id}")
    T_team_recruitment findById(int id);
    //指定编号的浏览量(+1)
    @Update("update t_team_recruitment set views=views+1 where id=#{id}")
    void addViews(int id);

//    指定编号的收藏量(+1)
    @Update("update t_team_recruitment set collection_volume = collection_volume+1 where id = #{id}")
    void addCollection(int id);

//    指定编号的收藏量(-1)
    @Update("update t_team_recruitment set collection_volume = collection_volume-1 where id = #{id}")
    void subCollection(int id);
}
