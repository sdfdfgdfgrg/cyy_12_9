package com.li.cyy.demo.controller;

import com.alibaba.fastjson.JSON;
import com.li.cyy.demo.bean.T_park_team;
import com.li.cyy.demo.bean.T_slideshow;
import com.li.cyy.demo.bean.T_team_recruitment;
import com.li.cyy.demo.mapper.T_UserMapper;
import com.li.cyy.demo.mapper.T_team_recruitmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/t_team_recruitment")
public class T_team_recruitmentController {
    @Autowired
    T_team_recruitmentMapper t_team_recruitmentMapper;

    @ResponseBody
    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public String findAllNotice(){
        List<T_team_recruitment> all = t_team_recruitmentMapper.findAll();

        String jsonList = JSON.toJSONString(all);
        System.out.println(jsonList);
        return jsonList;
    }
    @ResponseBody
    @RequestMapping(value = "/query_by_id", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public String findById(int id){
        T_team_recruitment byId = t_team_recruitmentMapper.findById(id);
        String jsonList = JSON.toJSONString(byId);
        System.out.println(jsonList);//111
        return jsonList;
    }
    //3.	增加指定编号的浏览量
    @ResponseBody
    @GetMapping(value = "/add_views")
    public boolean addViews(int id){
        t_team_recruitmentMapper.addViews(id);
        return true;
    }
    //4.	增加指定编号的收藏量(+1)
    @ResponseBody
    @GetMapping(value = "/add_collection_volume")
    public boolean addCollection(int id){
        t_team_recruitmentMapper.addCollection(id);
        return true;
    }
    //5.	减少指定编号的收藏量(-1)
    @ResponseBody
    @GetMapping(value = "/sub_collection_volume")
    public boolean subCollection(int id){
        t_team_recruitmentMapper.subCollection(id);
        return true;
    }



}
