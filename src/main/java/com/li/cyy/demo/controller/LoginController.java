package com.li.cyy.demo.controller;

import com.li.cyy.demo.bean.T_user;
import com.li.cyy.demo.mapper.T_UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class LoginController {
    @Autowired
    T_UserMapper t_userMapper;
    @RequestMapping("login.html")
    public String tologin(){
        return "login";
    }
    @RequestMapping("ifLogin")
    public String ifLogin(T_user t_user, Map<String,Object> map, HttpSession session){
        T_user t_user1 = t_userMapper.qUserByUserPWD(t_user);
//        System.out.println(t_user1.getUsername());
        if(t_user1!=null){
            session.setAttribute("user",t_user);
            return "redirect:/t_notice/dashboard";
        }else{
            map.put("msg","用户名或密码错误");
            return "redirect:/login.html";
        }
    }
}
