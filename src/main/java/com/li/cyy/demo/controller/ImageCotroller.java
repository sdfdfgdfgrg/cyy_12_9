package com.li.cyy.demo.controller;

import com.li.cyy.demo.mapper.ImageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
public class ImageCotroller {
    @Autowired
    ImageMapper imageMapper;

    @RequestMapping("/image")
    public String gotoImage(){
        return "imageUpload";
    }
    @PostMapping(value = "/fileUpload")
    public String fileUpload(@RequestParam(value = "file") MultipartFile file,
                             @RequestParam(value = "name")String name,
                             Model model, HttpServletRequest request) {
        if (file.isEmpty()) {
            System.out.println("文件为空空");
        }
        String fileName = file.getOriginalFilename();  // 文件名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));  // 后缀名
        //获取类加载的根路径
        System.out.println(this.getClass().getResource("/").getPath());
        String filePath = "C:\\Users\\86156\\Desktop\\练习\\比赛+项目\\cyy\\src\\main\\resources\\static\\image\\"; // 上传后的路径
        fileName = UUID.randomUUID() + suffixName; // 新文件名
        File dest = new File(filePath + fileName);
        if (!dest.getParentFile().exists()) {//存储
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String filename = "/static/image/" + fileName;
        System.out.println(filename);
        model.addAttribute("msg", "上传成功");
        imageMapper.addImage(name,filename);
        return "imageUpload";
    }
    @RequestMapping("layui")
    public String lay(){
        return "layui";
    }
}
