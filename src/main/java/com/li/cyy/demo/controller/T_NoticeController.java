package com.li.cyy.demo.controller;
import com.alibaba.fastjson.JSON;
import com.li.cyy.demo.bean.T_notice;
import com.li.cyy.demo.mapper.NoticeMapper;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Controller
@RequestMapping("/t_notice")
public class T_NoticeController {
    @Autowired
    NoticeMapper noticeMapper;
    @ResponseBody
    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public String findAllNotice(){
        List<T_notice> t_notices = noticeMapper.findAll();
//        model.addAttribute("notices",t_notices);
        String jsonList = JSON.toJSONString(t_notices);
        System.out.println(jsonList);
        return jsonList;
    }
    @RequestMapping("/dashboard")
    public String findAll(Model model){
        List<T_notice> notices  = noticeMapper.findAll();
        model.addAttribute("notices",notices);
        return "dashboard";
    }
    @RequestMapping("doDeleteById/{id}")
    public String del_notice(Model model, @PathVariable int id){
        int delete = noticeMapper.delNotice(id);
        return "redirect:/t_notice/dashboard";
    }
    @RequestMapping("doUpdateById/{id}")
    public String upd_notice(Model model, @PathVariable int id){
        T_notice t_notice = noticeMapper.findById(id);
        model.addAttribute("t_notice",t_notice);
        return "t_notice_update";
    }
//    @RequestMapping("doUpdateNotice")
//    public String doUpdateT_notice(@RequestParam("id") int id,
//                                   @RequestParam("text_content") String text_content,
//                                   @RequestParam("article_url") String article_url){
//        noticeMapper.update(id,text_content,article_url);
//        return "redirect:/t_notice/dashboard";
//    }
    @RequestMapping("doUpdateNotice2")
    public String doUpdateT_notice2(T_notice t_notice){
        noticeMapper.update2(t_notice);
        return "redirect:/t_notice/dashboard";
    }
    @RequestMapping("toAddNotice")
    public String toAddNotice(){
        return "t_notice_add";
    }
    @RequestMapping("addNotice")
    public String addT_notice(T_notice t_notice){
        noticeMapper.add(t_notice);
        return "redirect:/t_notice/dashboard";
    }

}
