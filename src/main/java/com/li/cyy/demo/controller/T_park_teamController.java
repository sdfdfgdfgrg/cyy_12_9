package com.li.cyy.demo.controller;

import com.alibaba.fastjson.JSON;
import com.li.cyy.demo.bean.T_park_team;
import com.li.cyy.demo.mapper.T_park_teamMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/t_park_team")
public class T_park_teamController {
    @Autowired
    T_park_teamMapper t_park_teamMapper;
    @ResponseBody
    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public String findAllPartTeam(){
        List<T_park_team> all = t_park_teamMapper.findAll();
        String jsonList = JSON.toJSONString(all);
        System.out.println(jsonList);//111
        return jsonList;
    }
    @ResponseBody
    @RequestMapping(value = "/query_by_id", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public String findById(int id){
        T_park_team t_park_team = t_park_teamMapper.findById(id);
        String jsonList = JSON.toJSONString(t_park_team);
        System.out.println(jsonList);//111
        return jsonList;
    }
    @ResponseBody
    @RequestMapping(value = "/query_by_type", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public String findByType(String  type){
        List<T_park_team> byType = t_park_teamMapper.findByType(type);
        String jsonList = JSON.toJSONString(byType);
        System.out.println(jsonList);//111
        return jsonList;
    }
    @RequestMapping("/queryAll")//查询所有团队信息
    public String findAll(Model model){
        List<T_park_team> t_park_teams = t_park_teamMapper.findAll();
        model.addAttribute("teams",t_park_teams);
        return "t_park_team";
    }
    @RequestMapping("/page")
    public String findPage(String pageNumber,Model model){
        String spPage=pageNumber;
        int pageSize = 5;//每页大小
        int pageNo=0;//页数
        if(spPage==null){
            pageNo=1;
        }else {
            pageNo = Integer.valueOf(spPage);
            if (pageNo < 1) {
                pageNo = 1;
            }
        }
        //设置最大页数
        int totalCount=0;
        int count=t_park_teamMapper.getCount();
        if(count>0){
            totalCount=count;
        }
        int maxPage=totalCount%pageSize==0?totalCount/pageSize:totalCount/pageSize+1;
        if(pageNo>maxPage){//上届
            pageNo=maxPage;
        }
        List<T_park_team> t_park_teams = t_park_teamMapper.findPage(pageNo,pageSize);
        model.addAttribute("pageNo",pageNo);
        model.addAttribute("pageSize",pageSize);
        model.addAttribute("maxPage",maxPage);
        model.addAttribute("teams",t_park_teams);
        return "t_park_team1";
    }
    @RequestMapping("doDeleteById/{id}")
    public String del_team(Model model, @PathVariable int id){
        int delete = t_park_teamMapper.delTeam(id);
        return "redirect:/t_park_team/queryAll";
    }
    @RequestMapping("doUpdateById/{id}")
    public String upd_team(Model model, @PathVariable int id){
        T_park_team t_park_team = t_park_teamMapper.findById(id);
        model.addAttribute("t_park_team",t_park_team);
        return "t_park_team_update";
    }
    public String upload(MultipartFile file){//上传图片到服务器
        String fileName = file.getOriginalFilename();  // 文件名
        String filePath = "/www/wwwroot/cyy.xyzme.ink/images/team/"; // 上传后的路径
        String suffixName = fileName.substring(fileName.lastIndexOf("."));  // 后缀名
        fileName = UUID.randomUUID() + suffixName; // 新文件名
        File dest = new File(filePath + fileName);//路径
//        if (!dest.getParentFile().exists()) {//存储
//            dest.getParentFile().mkdirs();
//        }
//        try {
//            file.transferTo(dest);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        return fileName;
    }
    @RequestMapping("doUpdateTeam")
    public String doUpdate_team(@RequestParam("id") int id,
                                   @RequestParam("name") String name,
                                   @RequestParam("type") String type,
                                   @RequestParam("logo_file") MultipartFile logofile    ,
                                   @RequestParam("room_file") MultipartFile room_file,
                                   @RequestParam("main_direction") String main_direction){
        String fileName = logofile.getOriginalFilename();  // 文件名
        String fileName2 = room_file.getOriginalFilename();  // 文件名2
        if(!fileName.equals("")){//logo被修改
            fileName = upload(logofile);
            t_park_teamMapper.update1(id,name,type,fileName,main_direction);
        }
        if(!fileName2.equals("")){//room图被修改
            fileName2 = upload(room_file);
            t_park_teamMapper.update2(id,name,type,fileName2,main_direction);
        }
        return "redirect:/t_park_team/queryAll";
    }
    @RequestMapping("toAddTeam")
    public String toAddTeam(){
        return "t_park_team_add";
    }
    @RequestMapping("addTeam")
    public String addT_team(@RequestParam("name") String name,
                            @RequestParam("type") String type,
                            @RequestParam("logo_file") MultipartFile logo_file,
                            @RequestParam("room_file") MultipartFile room_file,
                            @RequestParam("main_direction") String main_direction){
        String fileName = logo_file.getOriginalFilename();  // 文件名logo
        String fileName2 = room_file.getOriginalFilename();  // 文件名room
        if(!fileName.equals("")&&!fileName2.equals("")){//图片非空
            fileName = upload(logo_file);
            fileName2 = upload(room_file);
            t_park_teamMapper.add(name,type,fileName,fileName2,main_direction);
        }
        return "redirect:/t_park_team/queryAll";
    }
}
