package com.li.cyy.demo.controller;

import com.alibaba.fastjson.JSON;
import com.li.cyy.demo.bean.T_notice;
import com.li.cyy.demo.bean.T_slideshow;
import com.li.cyy.demo.mapper.NoticeMapper;
import com.li.cyy.demo.mapper.T_slideshowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class T_slideshowController {
    @Autowired
    T_slideshowMapper t_slideshowMapper;
    @ResponseBody
    @RequestMapping(value = "/t_slide", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public String findAllNotice(){
        List<T_slideshow> all = t_slideshowMapper.findAll();

        String jsonList = JSON.toJSONString(all);
        System.out.println(jsonList);
        return jsonList;
    }
}
