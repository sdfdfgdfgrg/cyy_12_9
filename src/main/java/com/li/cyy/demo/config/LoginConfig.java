package com.li.cyy.demo.config;

import com.li.cyy.demo.interceptor.AdminInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
@Configuration
public class LoginConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(new AdminInterceptor());
        registration.addPathPatterns("/**");//拦截所有路径
        registration.excludePathPatterns(
                "/**",//释放所有路径
                "login.html",            //登录
                "/**/*.html",            //html静态资源
                "/**/*.js",              //js静态资源
                "/**/*.css",             //css静态资源
                "/**/*.woff",
//                "/t_notice/*",
//                "/t_park_team/*",
//                "/t_slide",
                "/ifLogin",
                "/**/*.ttf",
                "/asserts/**",
                "/image/**",
                "/layui/**"
        );
    }
}
